// Main Socket Module
import _ from 'lodash';
import cypht from 'cyphtjs';
import basex from 'base-x';
import msgpack from 'msgpack-lite';
import * as config from '../../config';
import EventEmitter from 'eventemitter3';

const BASE91 = basex(
  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!#$%&()*+,./:;<=>?@[]^_`{|}~"'
);
const BASE61 = basex('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789');

const socketEvents = new EventEmitter();
socketEvents.on('send', (message, callback) => {
  const packedMsg = msgpack.encode(message);
  cypht.encypht(packedMsg, clientKeys.serverKey).then( encyphtedMsg => {
    socket.send(BASE91.encode(encyphtedMsg));
    if (typeof callback !== 'undefined') {
      callback();
    }
  });
});

socketEvents.on('init', () => {
  ( async function() {
    await establishSocketChannel();
  })();
});

let clientKeys = {};
let serverKey = new cypht.CyphtPublicKey();
let socket = null;

function establishSocketChannel() {
  try {
    cypht.generateKeys().then( keys => {
      socketEvents.emit('secure');
      clientKeys = keys;
      const url = `ws://${config.urls.socketServer}/${BASE61.encode(keys.publicKey.exportRaw())}`;
      console.log('connecting to', url);
      socket = new WebSocket(url);
      socket.addEventListener('open', () => {
        socketEvents.emit('open');
      });
      socket.addEventListener('close', e => {
        socketEvents.emit('close');
      });
      socket.addEventListener('error', e => {
        console.log('SOCKET ERROR', e);
        socketEvents.emit('error', e);
      });
      socket.addEventListener('message', e => {
        cypht.decypht(BASE91.decode(e.data), clientKeys.privateKey).then(packedMsg => {
          const msg = msgpack.decode(packedMsg);
          if (typeof msg.type !== 'undefined') {
            msg.type = `SOCK/${msg.type}`;
            socketEvents.emit(msg);
          } else {
            console.log('INVALID socket command', msg);
          }
        });
      });
    });
  } catch (error) {
    console.log('connect error', error);
  }
}

export {
  socketEvents as default
}