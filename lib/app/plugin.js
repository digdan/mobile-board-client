// Generic plugin to register sagas/reducers/actions
import _ from 'lodash';

const plugin = modules =>
  new Promise((resolve, reject) => {
    let stores = {};
    let scenes = {};

    _.forOwn(modules, (module, moduleName) => {
      if (typeof module.store !== 'undefined') {
        stores[moduleName] = new module.store;
      }
      if (typeof module.scene !== 'undefined') {
        scenes[moduleName] = module.scene;
      }
    });
    resolve({
      stores,
      scenes
    });
  });

export { plugin as default };
