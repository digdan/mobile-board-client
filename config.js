const assets = {
  images: [
    //require('./assets/images/source/image-profile-1.jpg'),
  ],
  fonts: [
    //'opensans-light': require('./assets/fonts/opensans-light.ttf'),
  ]
};

const colorTheme = {
  darkest: '#271556',
  dark: '#2739B2',
  light: '#686CE2',
  lightest: '#2DA8D8',
  accent: '#A4DD40'
};

const urls = {
  socketServer: 'crazyfreak.ddns.net:8080',
};

export { assets, urls };
