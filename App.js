import React from 'react';
import { Provider } from 'mobx-react';
import { mapping, light as lightTheme } from '@eva-design/eva';
import { ApplicationProvider } from 'react-native-ui-kitten';
import { Router, Stack, Scene } from 'react-native-router-flux';
import moduleLoader from './modules';
import AppLoader from './components/AppLoader';
import { assets } from './config';


/*
const moduleLoader = () => {
  return new Promise( (resolve, reject) => {
    resolve({
      scenes: {},
      stores: {}
    });
  });
}
*/

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loadedModules: {}
    };
  }

  componentDidMount() {
    moduleLoader().then( loadedModules => {
      this.setState({ loadedModules });
    });
  }

  render() {
    if (Object.keys(this.state.loadedModules).length === 0) {
      return null;
    }
    return (
      <AppLoader assets={assets}>
        <ApplicationProvider
          mapping={mapping}
          theme={lightTheme}
        >
          <Provider {...this.state.loadedModules.stores}>
            <Router>
              <Stack key="root">
                {Object.keys(this.state.loadedModules.scenes).map( sceneKey => this.state.loadedModules.scenes[sceneKey])}
              </Stack>
            </Router>
          </Provider>
        </ApplicationProvider>
      </AppLoader>
    );
  }
}

export default App;
