import React from 'react';
import { observable, action, asStructure } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Text, View, StyleSheet } from 'react-native';
import { Spinner } from 'react-native-ui-kitten';
import socketEvents from '../lib/socket';
import { Scene } from 'react-native-router-flux';

class socketStore {
  @observable status = {
    initialized: false,
    connected: false,
    keysGenerated: false,
  }

  @action
  initialized(value) {
    this.status.initialized = value;
  }

  @action
  connected(value) {
    this.status.connected = value;
  }

  @action
  keysGenerated(value) {
    this.status.keysGenerated = value;
  }

}

@inject('socket')
@observer
class SocketList extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    socketEvents.on('open', () => {
      console.log(Date.now(), 'socket open');
      this.props.socket.connected(true);
    }, this);
    socketEvents.on('secure', () => {
      console.log(Date.now(), 'socket secured');
      this.props.socket.keysGenerated(true);
    }, this);
    /*
    setTimeout( () => {
      socketEvents.emit('init');
    }, 200);
    this.props.socket.initialized(true);
    */
  }

  render() {
    return (
      <View style={styles.container}>
        <Text> Initialized : {this.props.socket.status.initialized ? 'True' : 'False'} </Text>
        <Text> Keys Generated : {this.props.socket.status.keysGenerated ? 'True' : 'False'} </Text>
        <Text> Connected : {this.props.socket.status.connected ? 'True' : 'False'} </Text>
        <Spinner />
      </View>
    );  
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
});

const SocketScene = (
  <Scene key={'socketList'} component={SocketList} title={'Connecting to host'} />
);


export {
  socketStore as store,
  SocketScene as scene
}