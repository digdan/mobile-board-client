import React from 'react';
import { observable, action, asStructure } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Text, View, StyleSheet } from 'react-native';
import { Spinner, Button } from 'react-native-ui-kitten';
import { Actions } from 'react-native-router-flux';

const AuthSignup = props => {
  return (
    <View>
      <Text>Signup</Text>
      <Button onPress={() => Actions.replace('signin')}>Sign in</Button>
    </View>
  );
}

export {
  AuthSignup as default
}