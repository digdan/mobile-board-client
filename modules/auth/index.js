import React from 'react';
import { observable, action, asStructure } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Text, View, StyleSheet } from 'react-native';
import { Spinner, Button } from 'react-native-ui-kitten';
import { Scene, Actions, Stack } from 'react-native-router-flux';
import AuthSignin from './signin';
import AuthSignup from './signup';

class authStore {
  @observable accounts=[];
  @observable selectedAccount=null;
  @action loadAccounts() {
    this.accounts = [];
    //TODO load accounts from local storage
    this.accounts.push({
      id: 1,
      name: 'test 1'
    });
    this.accounts.push({
      id: 2,
      name: 'test 2'
    });
  }
  @action selectAccount(selectedAccount) {
    this.selectedAccount = selectedAccount;
  }
}

const authScene = (
  <Stack key='authroot' hideNavBar={true}>
    <Scene key={'signin'} component={AuthSignin} initial={true} hideNavBar={false} title={'Sign In'} />
    <Scene key={'signup'} component={AuthSignup} hideNavBar={false} title={'Sign Up'} />
  </Stack>
);

export {
  authStore as store,
  authScene as scene
}