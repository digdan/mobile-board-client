import React from 'react';
import { observable, action, asStructure } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Text, View, StyleSheet } from 'react-native';
import { Spinner, Button, Select } from 'react-native-ui-kitten';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';

@inject('auth')
@observer 
class AuthSignin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedAccount: null
    }
  }
  onAccountSelect = selectedOption => {
    this.props.auth.selectAccount(selectedOption);
  }
  componentDidMount() {
    //Pull list of accounts from local storage
    this.props.auth.loadAccounts();
  }
  render() {
    const accounts = _.get(this.props, 'auth.accounts', []).map( account => {
      return {
        id: account.id,
        text: account.name
      }
    });
    return (
      <View>
        <Text>Sign In</Text>
        <Select
          data={accounts}
          selectedOption={this.props.auth.selectedAccount}
          onSelect={this.onAccountSelect}

        />
        <Button onPress={() => console.log('login id', this.props.auth.selectedAccount.id)} disabled={_.isNil(this.props.auth.selectedAccount)}>Login</Button>
        <Button onPress={() => Actions.signup()} disabled={(accounts.length > 2)} appearance='ghost'>Create New Account</Button>
      </View>
    );
  }
}

export {
  AuthSignin as default
}