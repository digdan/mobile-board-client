import React from 'react';
import { ImageRequireSource } from 'react-native';
import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';

class AppLoader extends React.Component {
  state = {
    loaded: false,
  }

  onLoadSuccess = () => {
    this.setState({
      loaded: true
    });
  };

  loadResources({images, fonts}) {
    return async function() {
      let cache = [];
      if (images.length > 0) {
        cache = images.map( image => {
          return Asset.fromModule(image).downloadAsync();
        });
      }
      if (fonts.length > 0) {
        cache.push(Font.loadAsync(fonts));
      }
      return Promise.all(cache);
    }
  };

  render = () => {
    if (this.state.loaded) {
      return this.props.children;
    }
    return( <AppLoading
      startAsync={this.loadResources(this.props.assets)}
      onFinish={this.onLoadSuccess}
      onError={console.error}
    /> );
  }
}

export {
  AppLoader as default
}
