// src/StoriesList.js

import React from 'react';
import {
  TouchableOpacity, Row, View,
}
import { observer, inject } from 'mobx-react';

import moment from 'moment';

const Story = inject('store')(
  observer(({ store, id }) => {
    const item = store.items.get(id);
    if (item) {
      return (
        <div>TEST</div>
      );
    }
    return (
      <div>Loading</div>
    );
  })
);

const StoriesList = inject('store')(
  observer(({ store, storyType }) => {
    const stories = store.stories.get(storyType);
    return <Spinner />;
  })
);

export default StoriesList;
