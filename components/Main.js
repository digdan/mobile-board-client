import React from 'react';
import { observer, inject } from 'mobx-react';
import { Text, View, StyleSheet } from 'react-native';
import { Spinner } from 'react-native-ui-kitten';
import { Actions } from 'react-native-router-flux';
import socketEvents from '../lib/socket';
import Dashboard from './Dashboard';

class Main extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    console.log('main mounted');
    socketEvents.on('open', () => { console.log('socket open')});
    socketEvents.on('secure', () => { console.log('socket secured')});
    socketEvents.emit('init');
  }

  render() {
    return (
      <View style={styles.container}>
        <Spinner />
      </View>
    );  
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
});

export default Main;
