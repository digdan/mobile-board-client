import React from 'react';
import { Text, View } from 'react-native';
import Toast from 'react-native-easy-toast';
import { RkTheme, RkComponent } from 'react-native-ui-kitten';

class RNToast extends RkComponent {
  componentName = 'Toast';

  constructor(props) {
    super(props);
    this.lastToast = 0;
    this.toast = null;
    this.toast.close();
    this.toast.show('Test', 1500);
  }

  render() {
    return <Toast opacity={0.8} ref={e => this.toast = e} />;
  }
}

export default RNToast;
